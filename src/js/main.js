var owl = $("#car_slider");

$(document).ready(function() {
  owl.owlCarousel({
	autoWidth: true,
	autoplay: true,
    margin: 60,
    center: true,
    loop: true,
  });

  $(".slider__btn--next").click(function() {
    owl.trigger("next.owl.carousel", [300])
  });

  $(".slider__btn--prev").click(function() {
    owl.trigger("prev.owl.carousel", [300])
  });
});
