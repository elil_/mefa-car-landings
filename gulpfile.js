'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
	postcss = require('gulp-postcss'),
	posthtml = require('gulp-posthtml'),
	autoprefixer = require('autoprefixer'),
	include = require('posthtml-include'),
	csso = require('gulp-csso'),
	cheerio = require('gulp-cheerio'),
	svgo = require('gulp-svgo'),
	svgstore = require('gulp-svgstore'),
	rename = require('gulp-rename'),
	replace = require('gulp-replace'),
	del = require('del');

gulp.task('css', () => {
    return gulp.src('src/scss/style.scss')
        .pipe(plumber())
		.pipe(sass())
		.pipe(postcss([
			autoprefixer()
		]))
		.pipe(gulp.dest('build/css'))
		.pipe(csso())
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest('build/css'));
});

gulp.task('svgSprite', () => {
	return gulp.src('src/img/svg/*.svg')
		.pipe(svgstore({
			inlineSvg: true
		}))
		.pipe(rename('sprite.svg'))
		.pipe(gulp.dest('build/img/svg'))
});

gulp.task('html', () => {
	return gulp.src('src/*.html')
		.pipe(posthtml([
			include()
		]))
		.pipe(gulp.dest('build'));
});

gulp.task('clean', () => {
	return del('build');
});

gulp.task('cleanTrash', () => {
	return del(['build/header.html', 'build/footer.html']).then(paths => {
		console.log('Deleted files:\n', paths.join('\n'));
	});
});

gulp.task('copy', () => {
    return gulp.src([
		'src/fonts/**/*.{woff,woff2}',
		'src/img/**.jpg',
		'src/img/**.png',
		'src/img/svg/**',
		'src/js/**.js',
		'src/owlcarousel/**.{css,js}'
	], {
		base: 'src'
	})
    	.pipe(gulp.dest('build'));
});

gulp.task('build', gulp.series('clean', 'copy', 'css', 'svgSprite', 'html', 'cleanTrash'));
